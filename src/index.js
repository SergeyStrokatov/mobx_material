import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import DevTools from 'mobx-react-devtools';
import {observer} from 'mobx-react';
import {observable, computed, configure, action, decorate} from 'mobx';


import {Input, TableRow, TableHead, TableContainer, TableCell, TableBody, Table, Button, Typography, Box} from '@material-ui/core';

import Paper from '@material-ui/core/Paper';


configure({enforceActions: 'observed'})

class Store {
    devList = [
        {name: 'jack', sp: 12},
        {name: 'max', sp: 10},
        {name: 'leo', sp: 8}
    ];

    filter = '';

    get totalSum() {
        return this.devList.reduce((acc, {sp}) => acc += sp, 0)
    };

    get topPerformer() {
        const max = Math.max(...this.devList.map(({sp}) => sp));
        return this.devList.find(({sp, name}) => {
            if (max === sp) {
                return name
            }
        })
    };

    get filterDevelopers() {
        const mathesFilter = new RegExp(this.filter, 'i')
        return this.devList.filter(({name}) => !this.filter || mathesFilter.test(name))
    }


    clearList() {
        this.devList = [];
    };

    addDeveloper(dev) {
        if (typeof dev.name === 'string' && Number.isInteger(dev.sp)) {
            this.devList.push(dev)
        }
    }

    updateFilter(value) {
        this.filter = value
    }
}

decorate(Store, {
    devList:          observable,
    filter:           observable,
    totalSum:         computed,
    topPerformer:     computed,
    filterDevelopers: computed,
    clearList:        action,
    addDeveloper:     action,
    updateFilter:     action
})

const appStore = new Store();


@observer
class CustomTable extends Component {
    render() {
        const {store} = this.props;

        return (
            <div style={{maxWidth: '500px'}}>
                <TableContainer component={Paper}>
                    <Table aria-label="simple table" style={{minWidth: '350px', maxWidth: '500px'}}>
                        <TableHead>
                            <TableRow>
                                <TableCell>Имя :</TableCell>
                                <TableCell>Очки :</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {store.filterDevelopers.map(({name, sp}, i) => (
                                <TableRow key={i}>
                                    <TableCell>{name}</TableCell>
                                    <TableCell>{sp}</TableCell>
                                </TableRow>
                            ))}
                            <TableRow>
                                <TableCell>Командный балл:</TableCell>
                                <TableCell>{store.totalSum}</TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>Лучший разработчик : </TableCell>
                                <TableCell>{store.topPerformer ? store.topPerformer.name : ""}</TableCell>
                            </TableRow>

                        </TableBody>
                    </Table>
                </TableContainer>
            </div>
        )
    }
}


@observer
class Controls extends Component {
    addDeveloper = () => {
        const name = prompt('the name: ');
        const sp   = parseInt(prompt('the story points: ', 10));
        this.props.store.addDeveloper({name, sp});
    }

    clearList = () => {
        this.props.store.clearList();
    }

    filterDevelopers = ({target: {value}}) => {
        this.props.store.updateFilter(value)
    }

    render() {
        return (
            <div className="controls">
                <Box m={2}>
                    <Button onClick={this.clearList} variant="contained" color="primary">очистить результаты</Button>
                </Box>
                <Box m={2}>
                    <Button onClick={this.addDeveloper} variant="contained" color="secondary">добавить разработчика</Button>
                </Box>
                <Box m={2}>
                    <Input color={'primary'} value={this.props.store.filter} onChange={this.filterDevelopers}/>
                </Box>
            </div>
        );
    }
}


class App extends Component {
    render() {
        return (
            <Box m={2}>

                <Box mt={3} mb={3}>
                    <Typography margin={'dense'} variant={'h4'}>Результаты спринта:</Typography>
                </Box>
                <Controls store={appStore}/>
                <CustomTable store={appStore}/>
            </Box>
        );
    }
}


ReactDOM.render(
    <React.StrictMode>
        <App store={appStore}/>
    </React.StrictMode>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
